#!/bin/bash

WORKSHOP_HOME=/c/Sources/workshop-global

alias cdwh="cd $WORKSHOP_HOME"
alias cdws="cd $WORKSHOP_HOME/server"
alias cdwt="cd $WORKSHOP_HOME/touch"
alias cdwu="cd $WORKSHOP_HOME/utils"
alias cdww="cd $WORKSHOP_HOME/web"
