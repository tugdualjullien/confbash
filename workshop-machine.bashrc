#!/bin/bash
alias wm-up="cd /c/Sources/workshop-machine && vagrant up && ./connectTo.sh"
alias wm-down="cd /c/Sources/workshop-machine && vagrant halt"
